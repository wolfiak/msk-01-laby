package com.example.pacio.msk_1_lab;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckedTextView;
import android.widget.ListView;

import com.example.pacio.msk_1_lab.adapter.Adapter;
import com.example.pacio.msk_1_lab.model.Student;
import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.appindexing.Thing;
import com.google.android.gms.common.api.GoogleApiClient;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private List<Student> myStrings;
    private int index;
    private Adapter adapter;
    private List<Integer> lista = new ArrayList<>();
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (savedInstanceState != null) {
            index = savedInstanceState.getInt("index");
           // myStrings = savedInstanceState.getStringArrayList("myString");
        } else {
            myStrings = new ArrayList<>();
            index = 0;
        }
        Log.d("cykle", "onCreate");


        adapter = new Adapter(this, myStrings);
        ListView listView = (ListView) findViewById(R.id.listView);
        listView.setAdapter(adapter);
        //listView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
        Button buton = (Button) findViewById(R.id.button);
        Button button2 = (Button) findViewById(R.id.button2);

        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                for (int n = 0; n < lista.size(); n++) {
                    Student el = (Student) adapter.getItem(lista.get(n));
                  //  adapter.remove(el);
                    adapter.notifyDataSetChanged();
                }
                lista.clear();
            }
        });
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                CheckedTextView checkedTextView =
                        (CheckedTextView) view.findViewById(android.R.id.text1);
                checkedTextView.setChecked(!checkedTextView.isChecked());
                if (checkedTextView.isChecked()) {
                    Log.d("test", "dodaje: " + i);
                    boolean flaga = true;
                    for (int n = 0; n < lista.size(); n++) {
                        if (lista.get(n) == i) {
                            flaga = false;
                        }
                    }
                    if (flaga == true) {
                        lista.add(i);
                    }

                }

                // String el = adapter.getItem(i);
                //adapter.remove(el);
                // adapter.notifyDataSetChanged();
            }
        });

        buton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //  myStrings.add("item" + index);
                // index++;
                //adapter.notifyDataSetChanged();
                Intent i=new Intent(MainActivity.this,AddStudentActivity.class);
                startActivityForResult(i,1);
            }
        });

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

    public void onSaveInstanceState(Bundle savedInstanceState) {
        //savedInstanceState.putStringArrayList("myString", (ArrayList<String>) myStrings);
        savedInstanceState.putInt("index", index);

    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode,requestCode,data);
        if(requestCode == 1){
            if(resultCode == Activity.RESULT_OK){
                CharSequence czar=data.getCharSequenceExtra("result");
                CharSequence nazwa=data.getCharSequenceExtra("nazwa");
                CharSequence numer=data.getCharSequenceExtra("numer");
                if(czar!=null){
                    myStrings.add(new Student(czar.toString(),"test"));
                    adapter.notifyDataSetChanged();
                } else if(nazwa!=null && numer !=null){
                    myStrings.add(new Student(nazwa.toString(),numer.toString()));
                    adapter.notifyDataSetChanged();
                }
            }
        }
    }

    protected void onStart() {
        super.onStart();// ATTENTION: This was auto-generated to implement the App Indexing API.
// See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        Log.d("cykle", "onStart");
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        AppIndex.AppIndexApi.start(client, getIndexApiAction());
    }

    protected void onResume() {
        super.onResume();
        Log.d("cykle", "onResume");
    }

    protected void onPause() {
        super.onPause();
        Log.d("cykle", "onPause");
    }

    protected void onStop() {
        super.onStop();// ATTENTION: This was auto-generated to implement the App Indexing API.
// See https://g.co/AppIndexing/AndroidStudio for more information.
        AppIndex.AppIndexApi.end(client, getIndexApiAction());
        Log.d("cykle", "onStop");
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.disconnect();
    }

    protected void onDestroy() {
        super.onDestroy();
        Log.d("cykle", "onDestroy");
    }

    protected void onRestart() {
        super.onRestart();
        Log.d("cykle", "onRestart");
    }

    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    public Action getIndexApiAction() {
        Thing object = new Thing.Builder()
                .setName("Main Page") // TODO: Define a title for the content shown.
                // TODO: Make sure this auto-generated URL is correct.
                .setUrl(Uri.parse("http://[ENTER-YOUR-URL-HERE]"))
                .build();
        return new Action.Builder(Action.TYPE_VIEW)
                .setObject(object)
                .setActionStatus(Action.STATUS_TYPE_COMPLETED)
                .build();
    }
}
