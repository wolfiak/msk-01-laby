package com.example.pacio.msk_1_lab.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import android.widget.TextView;

import com.example.pacio.msk_1_lab.R;
import com.example.pacio.msk_1_lab.model.Student;

import java.util.List;

/**
 * Created by pacio on 24.10.2017.
 */

public class Adapter extends BaseAdapter {
    private Context kontekst;
    private List<Student> lista;

    public Adapter(Context kontekst, List<Student> lista) {
        this.kontekst = kontekst;
        this.lista = lista;
    }

    @Override
    public int getCount() {
        return lista.size();
    }

    @Override
    public Object getItem(int i) {
        return lista.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        View v=view;
        if(v==null){
            LayoutInflater lf;
            lf=LayoutInflater.from(kontekst);
            v=lf.inflate(R.layout.student_adapter,null);
        }
        Student s=(Student) getItem(i);

        if(s!=null){
            TextView tv=(TextView) v.findViewById(R.id.textView);
            TextView tv2=(TextView) v.findViewById(R.id.textView2);

            if(tv!=null){
                tv.setText(s.getImie());

            }
            if(tv2 !=null){
                tv2.setText(s.getNazwisko());
            }
        }

        return v;
    }
}
