package com.example.pacio.msk_1_lab;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.net.URI;

public class AddStudentActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_student);


        Button b3= (Button) findViewById(R.id.button3);

        b3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditText et= (EditText) findViewById(R.id.editText);
                Intent i=new Intent();
                i.putExtra("result",et.getText());
                setResult(Activity.RESULT_OK,i);

                finish();
            }
        });
        Button b4= (Button) findViewById(R.id.button4);
        b4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent pick=new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
                startActivityForResult(pick,1000);
            }
        });
    }

    public void onActivityResult(int reqCode, int resultCode, Intent data) {
        super.onActivityResult(reqCode, resultCode, data);

        if(reqCode==1000){
            if(resultCode==Activity.RESULT_OK){
                Uri content=data.getData();
                Cursor c= getContentResolver().query(content,null,null,null,null);
                if(c.moveToFirst()){
                    String nazwa=c.getString(c.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                    String numer=c.getString(c.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER));

                    Intent i=new Intent();
                    i.putExtra("nazwa",nazwa);
                    i.putExtra("numer",numer);
                    setResult(Activity.RESULT_OK,i);
                    finish();
                }
            }
        }
    }
}
